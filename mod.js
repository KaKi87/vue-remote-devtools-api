import { dirname } from 'path';
import { fileURLToPath } from 'url';
import { exec } from 'node:child_process';
import path from 'node:path';

import getRandomPort from './lib/getRandomPort.js';
import waitForPort from './lib/waitForPort.js';

const __dirname = dirname(fileURLToPath(import.meta.url));

export default () => new Promise((resolve, reject) => {
    (async () => {
        const port = await getRandomPort();
        waitForPort(port).then(() => resolve({ port }));
        exec(
            path.join(__dirname, './node_modules/.bin/vue-devtools'),
            {
                env: {
                    ...process.env,
                    'PORT': port
                }
            }
        );
    })().catch(reject);
});