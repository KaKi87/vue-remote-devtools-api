import net from 'node:net';

const isPortOpen = port => new Promise(resolve => {
    const
        socket = new net.Socket(),
        onError = () => {
            socket.destroy();
            resolve(false);
        };
    socket.setTimeout(0);
    socket.once('error', onError);
    socket.once('timeout', onError);
    socket.connect(
        port,
        'localhost',
        () => {
            socket.end();
            resolve(true);
        }
    );
});

export default port => new Promise((resolve, reject) => {
    (async function _(){
        if(await isPortOpen(port))
            resolve();
        else
            setTimeout(_, 0);
    })().catch(reject);
});