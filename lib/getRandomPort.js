import net from 'node:net';

export default () => new Promise(resolve => {
    const server = net.createServer();
    server.listen(
        0,
        () => {
            const { port } = server.address();
            server.close(() => resolve(port));
        }
    );
});